\select@language {french}
\contentsline {section}{Introduction}{4}
\contentsline {section}{\numberline {I}Description du projet et objectif}{4}
\contentsline {section}{\numberline {II}Avancement au cours de l'ann\IeC {\'e}e}{5}
\contentsline {section}{\numberline {III}Projet d'utilisation des logiciels IBM : SPSS et ODM}{6}
\contentsline {subsection}{\numberline {III.1}Pr\IeC {\'e}sentation succincte de SPSS}{6}
\contentsline {subsection}{\numberline {III.2}Pourquoi nous avons abandonn\IeC {\'e} SPSS}{6}
\contentsline {subsection}{\numberline {III.3}Pr\IeC {\'e}sentation et utilisation d'ODM}{7}
\contentsline {subsection}{\numberline {III.4}Difficult\IeC {\'e}s avec ODM}{7}
\contentsline {section}{\numberline {IV}Analyse th\IeC {\'e}orique du probl\IeC {\`e}me et mod\IeC {\'e}lisation}{8}
\contentsline {subsection}{\numberline {IV.1}D\IeC {\'e}termination statistique du th\IeC {\`e}me des messages}{8}
\contentsline {subsubsection}{\numberline {IV.1.1}Normalisation du texte}{8}
\contentsline {subsubsection}{\numberline {IV.1.2}Description de l'algorithme TfidfVectorizer}{8}
\contentsline {subsubsection}{\numberline {IV.1.3}Classification des tweets par th\IeC {\`e}me - Entropie maximum}{9}
\contentsline {paragraph}{Choix de la m\IeC {\'e}thode et de la biblioth\IeC {\`e}que}{9}
\contentsline {paragraph}{Notations}{10}
\contentsline {paragraph}{Mod\IeC {\`e}le logarithme-lin\IeC {\'e}aire}{10}
\contentsline {paragraph}{Estimation des coefficients}{11}
\contentsline {paragraph}{Quel rapport avec l'entropie?}{12}
\contentsline {subsection}{\numberline {IV.2}D\IeC {\'e}terminer le profil de l'utilisateur}{13}
\contentsline {subsubsection}{\numberline {IV.2.1}Principe d'\IeC {\'e}laboration du profil en 5 \IeC {\'e}tapes}{14}
\contentsline {subsubsection}{\numberline {IV.2.2}D\IeC {\'e}terminer le lien entre deux utlisateurs u et v}{16}
\contentsline {subsubsection}{\numberline {IV.2.3}Apprentissage statistique}{16}
\contentsline {paragraph}{Base de donn\IeC {\'e}es}{17}
\contentsline {paragraph}{Quelques m\IeC {\'e}thodes d'apprentissage statistique autre que la r\IeC {\'e}gression lin\IeC {\'e}aire}{17}
\contentsline {paragraph}{La r\IeC {\'e}gression lin\IeC {\'e}aire au service de la d\IeC {\'e}termination du profil d'un utilisateur}{18}
\contentsline {subsection}{\numberline {IV.3}Traitement des messages (Tweets)}{18}
\contentsline {subsubsection}{\numberline {IV.3.1}Pertinence d'un tweet}{18}
\contentsline {subsubsection}{\numberline {IV.3.2}Classification}{20}
\contentsline {section}{\numberline {V}Construction d'une base de donn\IeC {\'e}es}{21}
\contentsline {section}{\numberline {VI} Impl\IeC {\'e}mentation de l'application}{22}
\contentsline {subsection}{\numberline {VI.1}Java}{22}
\contentsline {subsection}{\numberline {VI.2}Utilisation des biblioth\IeC {\`e}ques Python}{22}
\contentsline {subsubsection}{\numberline {VI.2.1}Motivations}{22}
\contentsline {subsubsection}{\numberline {VI.2.2}Probl\IeC {\`e}mes d'int\IeC {\'e}grations Jython}{23}
\contentsline {subsection}{\numberline {VI.3}Impl\IeC {\'e}mentation en Java du traitement des messages}{23}
\contentsline {subsubsection}{\numberline {VI.3.1}Biblioth\IeC {\`e}ques Java : Stanford Classifier et Snowball}{23}
\contentsline {subsubsection}{\numberline {VI.3.2}Structure du projet Java}{23}
\contentsline {paragraph}{TwMParser}{23}
\contentsline {paragraph}{AbstractTweetParser}{24}
\contentsline {paragraph}{classifier}{24}
\contentsline {paragraph}{Tweet}{24}
\contentsline {paragraph}{themes}{24}
\contentsline {paragraph}{LinearRegression}{24}
\contentsline {paragraph}{profil}{24}
\contentsline {paragraph}{LastTweets}{24}
\contentsline {subsection}{\numberline {VI.4}Une application sous android}{25}
\contentsline {subsubsection}{\numberline {VI.4.1}Fonctionnalit\IeC {\'e}s principales de l'application}{25}
\contentsline {subsubsection}{\numberline {VI.4.2}Fonctionnement g\IeC {\'e}n\IeC {\'e}ral de l'application}{25}
\contentsline {subsubsection}{\numberline {VI.4.3}Int\IeC {\'e}gration des algorithmes de classement des Tweets et d'\IeC {\'e}laboration du profil}{26}
\contentsline {paragraph}{Contraintes mat\IeC {\'e}rielles, vitesse d'\IeC {\'e}x\IeC {\'e}cution}{26}
\contentsline {paragraph}{Contraintes li\IeC {\'e}es \IeC {\`a} Twitter}{27}
\contentsline {subsection}{\numberline {VI.5}Extraction des donn\IeC {\'e}es}{28}
\contentsline {subsubsection}{\numberline {VI.5.1}Connexion \IeC {\`a} Twitter}{28}
\contentsline {subsubsection}{\numberline {VI.5.2}Quelles donn\IeC {\'e}es extrait-on ?}{28}
\contentsline {subsubsection}{\numberline {VI.5.3}Stockage des donn\IeC {\'e}es}{28}
\contentsline {section}{\numberline {VII}R\IeC {\'e}sultats}{29}
\contentsline {subsubsection}{\numberline {VII.0.4}Efficacit\IeC {\'e} du classifieur}{29}
\contentsline {subsubsection}{\numberline {VII.0.5}Construction du profil}{29}
\contentsline {paragraph}{Variation de $\alpha $}{30}
\contentsline {paragraph}{Variation de $\beta $}{30}
\contentsline {paragraph}{Variation de $R_w$}{30}
\contentsline {paragraph}{Variation de $W_{ff}$}{30}
\contentsline {subsubsection}{\numberline {VII.0.6}Resultats finaux visibles par l'utilisateur}{31}
\contentsline {section}{Conclusion}{32}
\contentsline {section}{Bibliographie}{33}
